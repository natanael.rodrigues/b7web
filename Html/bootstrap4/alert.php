<!doctype html>
<html lang="pt-br">
  <head>
    <title>Portal Modelo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
   
  </head>
  <body>
     <div class="container">
        <div class="alert">
          Alerta comum
        </div>
        <hr>

        <div class="alert alert-primary" role="alert">
          Alerta comum
        </div>
        <hr>
        <div class="alert alert-secondary" role="alert">
          Alerta comum
        </div>
        <hr>
        <div class="alert alert-success" role="alert">
          Alerta comum
        </div>
        <hr>
        <div class="alert alert-danger" role="alert">
          Alerta comum
        </div>
        <hr>
        <div class="alert alert-warning " role="alert">
          Alerta comum
        </div>
        <hr>
        <div class="alert alert-info " role="alert">
          Alerta comum
        </div>
        <hr>
        <div class="alert alert-light " role="alert">
          Alerta comum
        </div>
        <hr>
        <div class="alert alert-dark " role="alert">
          Alerta comum
        </div>
        <hr>
        <div class="alert alert-danger " role="alert">
          <h4 class="alert-heading">Ocorreram erros</h4>
          Usuário e senhas incorretos
        </div>
        <hr>
        <div class="alert alert-danger alert-dismissible" role="alert">
          Usuário e senhas incorretos
          <button class="close" data-dismiss="alert" aria-label="Fechar">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <hr>
     </div> 

    <script src="assets/js/jquery-3.4.1.min.js" ></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
  </body>
</html>