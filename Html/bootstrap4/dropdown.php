<!doctype html>
<html lang="pt-br">
  <head>
    <title>Portal Modelo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" >
   
  </head>
  <body>
     <div class="container">
        
        <div class="dropdown">

          <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
            Editar
          </a>

          <div class="dropdown-menu">

            <a class="dropdown-item" href="http://google.com">Google</a>
            <a class="dropdown-item" href="http://facebook.com">Facebook</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="http://youtube.com">Youtube</a>

          </div>

        </div>

        <div class="dropdown dropright">

          <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
            Editar
          </a>

          <div class="dropdown-menu">

            <a class="dropdown-item" href="http://google.com">Google</a>
            <a class="dropdown-item" href="http://facebook.com">Facebook</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="http://youtube.com">Youtube</a>

          </div>

        </div>

        <div class="dropdown btn-group">
          <a href="http://crm.dalmark.com.br" class="btn btn-primary">CRM</a>
          <a href="#" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown"></a>

          <div class="dropdown-menu">

            <a class="dropdown-item" href="http://google.com">Google</a>
            <a class="dropdown-item" href="http://facebook.com">Facebook</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="http://youtube.com">Youtube</a>

          </div>

        </div>
        
     </div> 

    <script src="assets/js/jquery-3.4.1.min.js"> </script>
    <script src="assets/js/bootstrap.bundle.min.js" ></script>
  </body>
</html>