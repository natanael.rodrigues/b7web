<!doctype html>
<html lang="pt-br">
  <head>
    <title>Portal Modelo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   
  </head>
  <body>
     <div class="container">
        <table class="table">
          <tr>
            <th>#</th>
            <th>Nome</th>
            <th>Sobrenome</th>
            <th>Idade</th>
          </tr>
          <tr>
            <td>1</td>
            <td>Natanael Roberto</td>
            <td>Rodrigues</td>
            <td>38</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Luis</td>
            <td>Gustavo</td>
            <td>88</td>
          </tr>
          <tr>
            <td>3</td>
            <td>Jorge</td>
            <td>Algusto</td>
            <td>99</td>
          </tr>
        </table>
        <hr>

        <table class="table table-dark">
          <tr>
            <th>#</th>
            <th>Nome</th>
            <th>Sobrenome</th>
            <th>Idade</th>
          </tr>
          <tr>
            <td>1</td>
            <td>Natanael Roberto</td>
            <td>Rodrigues</td>
            <td>38</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Luis</td>
            <td>Gustavo</td>
            <td>88</td>
          </tr>
          <tr>
            <td>3</td>
            <td>Jorge</td>
            <td>Algusto</td>
            <td>99</td>
          </tr>
        </table>

        <hr>

        <table class="table table-striped">
          <tr>
            <th>#</th>
            <th>Nome</th>
            <th>Sobrenome</th>
            <th>Idade</th>
          </tr>
          <tr>
            <td>1</td>
            <td>Natanael Roberto</td>
            <td>Rodrigues</td>
            <td>38</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Luis</td>
            <td>Gustavo</td>
            <td>88</td>
          </tr>
          <tr>
            <td>3</td>
            <td>Jorge</td>
            <td>Algusto</td>
            <td>99</td>
          </tr>
          
        </table>


        <hr>

        <table class="table table-bordered">
          <tr>
            <th>#</th>
            <th>Nome</th>
            <th>Sobrenome</th>
            <th>Idade</th>
          </tr>
          <tr>
            <td>1</td>
            <td>Natanael Roberto</td>
            <td>Rodrigues</td>
            <td>38</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Luis</td>
            <td>Gustavo</td>
            <td>88</td>
          </tr>
          <tr>
            <td>3</td>
            <td>Jorge</td>
            <td>Algusto</td>
            <td>99</td>
          </tr>
        </table>

        <hr>

        <table class="table table-hover">
          <tr>
            <th>#</th>
            <th>Nome</th>
            <th>Sobrenome</th>
            <th>Idade</th>
          </tr>
          <tr>
            <td>1</td>
            <td>Natanael Roberto</td>
            <td>Rodrigues</td>
            <td>38</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Luis</td>
            <td>Gustavo</td>
            <td>88</td>
          </tr>
          <tr>
            <td>3</td>
            <td>Jorge</td>
            <td>Algusto</td>
            <td>99</td>
          </tr>
        </table>

        <hr>

        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th>#</th>
              <th>Nome</th>
              <th>Sobrenome</th>
              <th>Idade</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Natanael Roberto</td>
              <td>Rodrigues</td>
              <td>38</td>
            </tr>
            <tr>
              <td>2</td>
              <td>Luis</td>
              <td>Gustavo</td>
              <td>88</td>
            </tr>
            <tr>
              <td>3</td>
              <td>Jorge</td>
              <td>Algusto</td>
              <td>99</td>
            </tr>
          </tbody>
        </table>

        <hr>

        <table class="table">
          <thead class="thead-light">
            <tr>
              <th>#</th>
              <th>Nome</th>
              <th>Sobrenome</th>
              <th>Idade</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Natanael Roberto</td>
              <td>Rodrigues</td>
              <td>38</td>
            </tr>
            <tr>
              <td>2</td>
              <td>Luis</td>
              <td>Gustavo</td>
              <td>88</td>
            </tr>
            <tr>
              <td>3</td>
              <td>Jorge</td>
              <td>Algusto</td>
              <td>99</td>
            </tr>
          </tbody>
        </table>

        <hr>

        <table class="table table-sm">
          <thead class="thead-light">
            <tr>
              <th>#</th>
              <th>Nome</th>
              <th>Sobrenome</th>
              <th>Idade</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Natanael Roberto</td>
              <td>Rodrigues</td>
              <td>38</td>
            </tr>
            <tr>
              <td>2</td>
              <td>Luis</td>
              <td>Gustavo</td>
              <td>88</td>
            </tr>
            <tr>
              <td>3</td>
              <td>Jorge</td>
              <td>Algusto</td>
              <td>99</td>
            </tr>
          </tbody>
        </table>  
        <hr>
        <div class="table-resonsive">
      

          <table class="table table-sm">
              <thead class="thead-light">
                <tr>
                  <th>#</th>
                  <th>Nome</th>
                  <th>Sobrenome</th>
                  <th>Idade</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Natanael Roberto</td>
                  <td>Rodrigues</td>
                  <td>38</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Luis</td>
                  <td>Gustavo</td>
                  <td>88</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Jorge</td>
                  <td>Algusto</td>
                  <td>99</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Jorge</td>
                  <td>Algusto</td>
                  <td>99</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Jorge</td>
                  <td>Algusto</td>
                  <td>99</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Jorge</td>
                  <td>Algusto</td>
                  <td>99</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Jorge</td>
                  <td>Algusto</td>
                  <td>99</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Jorge</td>
                  <td>Algusto</td>
                  <td>99</td>
                </tr>
              </tbody>
            </table>         
          </div>  
        </div>      
     </div> 

    <script src="assets/js/jquery-3.4.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="assets/js/bootstrap.bundle.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>