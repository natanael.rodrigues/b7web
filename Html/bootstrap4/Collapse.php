<!doctype html>
<html lang="pt-br">
  <head>
    <title>Portal Modelo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" >
   
  </head>
  <body>
     <div class="container">
        
        <button class="btn btn-primary" data-toggle="collapse" data-target="#area">Mostrar/Esconder</button>  
        <div class="collapse" id="area">
          <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sodales, massa at volutpat vehicula, mauris erat placerat ligula, non sodales turpis massa id arcu. Nam malesuada tristique orci id dapibus. Etiam rhoncus metus quam, quis commodo lacus pellentesque id. Quisque tincidunt, nulla nec ornare viverra, nisl tellus tristique nibh, a euismod lacus odio eget ex. Etiam commodo tellus et pharetra lobortis. Sed a justo et orci hendrerit porta. Suspendisse sollicitudin risus sit amet eros elementum ullamcorper. Duis odio magna, luctus nec sem ut, maximus accumsan lacus.

          Integer dictum bibendum sapien, id consectetur eros tempor eget. Quisque eget elementum enim, non pharetra justo. Aenean mi mi, fermentum vitae felis eu, sodales semper odio. In hac habitasse platea dictumst. Maecenas quis nibh justo. Duis consectetur luctus feugiat. Sed viverra nunc convallis, feugiat justo eu, ornare tellus. Etiam vel leo nec erat tempus rutrum sit amet feugiat nunc. Curabitur aliquam ex erat, ac ullamcorper eros aliquam non. Sed varius blandit nibh. Vivamus aliquam tellus quis velit convallis, ac laoreet lorem ullamcorper. Vivamus non dolor in dolor elementum efficitur eget sed metus.
          </p>
        </div>
     </div> 

    <script src="assets/js/jquery-3.4.1.min.js"> </script>
    <script src="assets/js/bootstrap.bundle.min.js" ></script>
  </body>
</html>