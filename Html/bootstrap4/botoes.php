<!doctype html>
<html lang="pt-br">
  <head>
    <title>Portal Modelo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" >
   
  </head>
  <body>
     <div class="container">
        <a class="btn btn-primary" href="#"> Botão link Primary</a>
        <hr>   
        <button  class="btn btn-danger" >Botão Danger</button>
        <hr>   
        <input type="submit"  class="btn btn-secondary"  value="Botão submit Secondary">
        <hr>   
        <input type="submit"  class="btn btn-success"  value="Botão submit Success">
        <hr>   
        <input type="submit"  class="btn btn-warning"  value="Botão submit Warning">
        <hr>   
        <input type="submit"  class="btn btn-info"  value="Botão submit Info">
        <hr>   
        <input type="submit"  class="btn btn-light"  value="Botão submit Light">
        <hr>   
        <input type="submit"  class="btn btn-dark"  value="Botão submit Dark">
        <hr>   
        <input type="submit"  class="btn btn-link"  value="Botão submit link">
        <hr>   
        <input type="submit"  class="btn btn-outline-primary"  value="Botão">
        <hr>
        <input type="submit"  class="btn btn-primary btn-lg"  value="Botão">
        <input type="submit"  class="btn btn-primary btn-sm"  value="Botão">
        <hr>  
        <input type="submit"  class="btn btn-block btn-primary"  value="Botão">
        <hr> 
        <div class="btn-group">
          <input type="submit"  class="btn btn-primary"  value="Botão">
          <input type="submit"  class="btn btn-primary"  value="Botão">
          <input type="submit"  class="btn btn-primary"  value="Botão">
          <input type="submit"  class="btn btn-primary"  value="Botão">
          <input type="submit"  class="btn btn-success"  value="Botão">
          <input type="submit"  class="btn btn-warning"  value="Botão">
          <input type="submit"  class="btn btn-info"  value="Botão">
          <input type="submit"  class="btn btn-dark"  value="Botão">
        </div>
        <hr>
        <div class="btn-group-vertical">
          <input type="submit"  class="btn btn-primary"  value="Botão">
          <input type="submit"  class="btn btn-primary"  value="Botão">
          <input type="submit"  class="btn btn-primary"  value="Botão">
          <input type="submit"  class="btn btn-primary"  value="Botão">
          <input type="submit"  class="btn btn-success"  value="Botão">
          <input type="submit"  class="btn btn-warning"  value="Botão">
          <input type="submit"  class="btn btn-info"  value="Botão">
          <input type="submit"  class="btn btn-dark"  value="Botão">
        </div>
        <hr> 
     </div> 

    <script src="assets/js/jquery-3.4.1.min.js"> </script>
    <script src="assets/js/bootstrap.bundle.min.js" ></script>
  </body>
</html>