<!doctype html>
<html lang="pt-br">
  <head>
    <title>Portal Modelo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" >
   
  </head>
  <body>
     <div class="container">

        <div style="width:600px; margin: 20px;">
        
          <ul class="list-group">
            <li class="list-group-item">Item1</li>
            <li class="list-group-item">Item 2</li>
            <li class="list-group-item">Item 3</li>
          </ul>      
        
        </div>

        <hr>


        <div style="width:600px; margin: 20px;">
        
          <ul class="list-group">
            <li class="list-group-item active">Item1 habilitado</li>
            <li class="list-group-item">Item 2</li>
            <li class="list-group-item disabled">Item 3 desabilitado</li>
          </ul>      
        
        </div>

        <hr>


        
        <div style="width:600px; margin: 20px;">
        
          <div class="list-group list-group-flush">
            <a href="#" class="list-group-item list-group-item-action">Site 1</a>
            <a href="#" class="list-group-item list-group-item-action">Site 2</a>
            <a href="#" class="list-group-item list-group-item-action">Site 3</a>
          </div>      
        
        </div>

        <hr>


        <div style="width:600px; margin: 20px;">
        
        <div class="list-group list-group-flush">
          <a href="#" class="list-group-item list-group-item-action list-group-item-primary">Site 1</a>
          <a href="#" class="list-group-item list-group-item-action list-group-item-danger">Site 2</a>
          <a href="#" class="list-group-item list-group-item-action">Site 3</a>
        </div>      
      
      </div>

      <hr>



      <div style="width:600px; margin: 20px;">
        
        <ul class="list-group">
          <li class="list-group-item">
            Item 1
            <span class="badge badge-primary badge-pill">12</span>
          </li>
          <li class="list-group-item">Item 2</li>
          <li class="list-group-item">Item 3</li>
        </ul>      
      
      </div>

      <hr>


      <div style="width:600px; margin: 20px;">
        
        <ul class="list-group">
          <li class="list-group-item d-flex justify-content-between align-itens-center">
            Item 1
            <span class="badge badge-primary badge-pill">12</span>
          </li>
          <li class="list-group-item">Item 2</li>
          <li class="list-group-item">Item 3</li>
        </ul>      
      
      </div>

      <hr>

      <div style="width:600px; margin: 20px;">
        
        <div class="list-group">
          <a href="#" class="list-group-item list-group-item-action">
              <div class="d-flex justify-content-between">
                  <h5>Titulo da Notícia</h5>
                  <small> 2 dias atrás</small>
              </div>
              <p>
                  resulmo da notícia do titulo acima.
              </p>
          </a>
          <a href="#" class="list-group-item list-group-item-action">
              <div class="d-flex justify-content-between">
                  <h5>Titulo da Notícia</h5>
                  <small> 2 dias atrás</small>
              </div>
              <p>
                  resulmo da notícia do titulo acima.
              </p>              
          </a>
          <a href="#" class="list-group-item list-group-item-action">
              <div class="d-flex justify-content-between">
                  <h5>Titulo da Notícia</h5>
                  <small>2 dias atrás</small>
              </div>
              <p>
                    resulmo da notícia do titulo acima.
              </p>              
          </a>
          
        </div>      
      
      </div>

      <hr>
        
     </div> 

    <script src="assets/js/jquery-3.4.1.min.js"> </script>
    <script src="assets/js/bootstrap.bundle.min.js" ></script>
  </body>
</html>