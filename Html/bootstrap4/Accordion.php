<!doctype html>
<html lang="pt-br">
  <head>
    <title>Portal Modelo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" >
   
  </head>
  <body>
     <div class="container">
        <div id="accordion">
            <div class="card">
              <div class="card-header">
                <button class="btn btn-block" data-toggle="collapse" data-target="#c1" aria-controls="c1">Quem foi Cabral?</button>
              </div>
              <div id="c1" class="collapse show" data-parent="#accordion">
                <div class="card-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sodales, massa at volutpat vehicula, mauris erat placerat ligula, non sodales turpis massa id arcu. Nam malesuada tristique orci id dapibus. Etiam rhoncus metus quam, quis commodo lacus pellentesque id. Quisque tincidunt, nulla nec ornare viverra, nisl tellus tristique nibh, a euismod lacus odio eget ex. Etiam commodo tellus et pharetra lobortis. Sed a justo et orci hendrerit porta. Suspendisse sollicitudin risus sit amet eros elementum ullamcorper. Duis odio magna, luctus nec sem ut, maximus accumsan lacus.</div>
              </div>
            </div>

            <div class="card">
              <div class="card-header">
                <button class="btn btn-block" data-toggle="collapse" data-target="#c2" aria-controls="c2">Onde foi Cabral que nasceu?</button>
              </div>
              <div id="c2" class="collapse" data-parent="#accordion">
                <div class="card-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sodales, massa at volutpat vehicula, mauris erat placerat ligula, non sodales turpis massa id arcu. Nam malesuada tristique orci id dapibus. Etiam rhoncus metus quam, quis commodo lacus pellentesque id. Quisque tincidunt, nulla nec ornare viverra, nisl tellus tristique nibh, a euismod lacus odio eget ex. Etiam commodo tellus et pharetra lobortis. Sed a justo et orci hendrerit porta. Suspendisse sollicitudin risus sit amet eros elementum ullamcorper. Duis odio magna, luctus nec sem ut, maximus accumsan lacus.</div>
              </div>
            </div>

            <div class="card">
              <div class="card-header">
                <button class="btn btn-block" data-toggle="collapse" data-target="#c3" aria-controls="c3">Onde Mora Cabral?</button>
              </div>
              <div id="c3" class="collapse" data-parent="#accordion">
                <div class="card-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sodales, massa at volutpat vehicula, mauris erat placerat ligula, non sodales turpis massa id arcu. Nam malesuada tristique orci id dapibus. Etiam rhoncus metus quam, quis commodo lacus pellentesque id. Quisque tincidunt, nulla nec ornare viverra, nisl tellus tristique nibh, a euismod lacus odio eget ex. Etiam commodo tellus et pharetra lobortis. Sed a justo et orci hendrerit porta. Suspendisse sollicitudin risus sit amet eros elementum ullamcorper. Duis odio magna, luctus nec sem ut, maximus accumsan lacus.</div>
              </div>
            </div>
        </div>   
        
     </div> 

    <script src="assets/js/jquery-3.4.1.min.js"> </script>
    <script src="assets/js/bootstrap.bundle.min.js" ></script>
  </body>
</html>