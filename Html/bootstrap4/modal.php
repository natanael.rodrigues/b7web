<!doctype html>
<html lang="pt-br">
  <head>
    <title>Portal Modelo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" >
   
  </head>
  <body>
     <div class="container">
        
     <button class="btn btn-primary" data-toggle="modal" data-target="#janela-simples">Clique aqui para abrir simples</button>
     <button class="btn btn-primary" data-toggle="modal" data-target="#janela">Clique aqui para abrir</button>
     <button class="btn btn-primary" data-toggle="modal" data-target="#janela-lg">Clique aqui para abrir Larger</button>
     <button class="btn btn-primary" data-toggle="modal" data-target="#janela-sm">Clique aqui para abrir Pequeno</button>
     <hr>
     <button class="btn btn-primary" data-toggle="modal" data-target="#janela-fecha">Clique aqui para abrir com botão de fechar</button>
        


        <div class="modal fade" id='janela-simples'>
          <div class="modal-dialog">
            <div class="modal-content">
            
              <div class="modal-header">
                <H5 class="modal-title">Titulo do modal</H5>
              </div>

              <div class="modal-body">
                ...              
              </div>

              <div class="modal-footer">
              
              
              </div>



            </div>
          </div>
        </div>

        <div class="modal fade" id='janela'>
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            
              <div class="modal-header">
                <H5 class="modal-title">Titulo do modal</H5>
              </div>

              <div class="modal-body">
                ...              
              </div>

              <div class="modal-footer">
              
              
              </div>



            </div>
          </div>
        </div>

        <div class="modal fade" id='janela-lg'>
          <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
            
              <div class="modal-header">
                <H5 class="modal-title">Titulo do modal</H5>
              </div>

              <div class="modal-body">
                ...              
              </div>

              <div class="modal-footer">
              
              
              </div>



            </div>
          </div>
        </div>


        <div class="modal fade" id='janela-sm'>
          <div class="modal-dialog modal-dialog-centered modal-sm">
            <div class="modal-content">
            
              <div class="modal-header">
                <H5 class="modal-title">Titulo do modal</H5>
              </div>

              <div class="modal-body">
                ...              
              </div>

              <div class="modal-footer">
              
              
              </div>



            </div>
          </div>
        </div>

        <div class="modal fade" id='janela-fecha'>
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
            
              <div class="modal-header">
                <H5 class="modal-title">Titulo do modal</H5>
                <button class="close" data-dismiss="modal"><span>&times;</span></button>
              </div>
              <div class="modal-body">
                ...              
              </div>
              <div class="modal-footer">
              
                <button class="btn btn-danger" data-dismiss="modal">Fechar Janela</button>
             
              </div>
            </div>
          </div>
        </div>


     </div> 

    <script src="assets/js/jquery-3.4.1.min.js"> </script>
    <script src="assets/js/bootstrap.bundle.min.js" ></script>
  </body>
</html>