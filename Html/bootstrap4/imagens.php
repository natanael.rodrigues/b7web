<!doctype html>
<html lang="pt-br">
  <head>
    <title>Portal Modelo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   
  </head>
  <body>
     <div class="container">

      <img src="assets/images/wallpaper.png" class="img-fluid"/>   

      <hr>
      <img src="assets/images/me.jpeg" class="img-thumbnail"/>   

      <hr>
      <img src="assets/images/me.jpeg" class="rounded"/>   

      <hr>
      <img src="assets/images/me.jpeg" class="rounded float-right"/>  
      <hr>
      <figure class="figure">
        <img src="assets/images/me.jpeg" class="figure-img img-fluid rounded img-thumbnail"/> 
        <figcaption class="figure-caption text-right">Natanael Roberto Rodrigues</figcaption>
      </figure>

      <hr>
   
     </div> 

    <script src="assets/js/jquery-3.4.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="assets/js/bootstrap.bundle.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>