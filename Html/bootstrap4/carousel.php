<!doctype html>
<html lang="pt-br">
  <head>
    <title>Portal Modelo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" >
   
  </head>
  <body>
     <div class="container">
        
        <div class="slide carousel" id="slideshowExemplo">
            <ol class="carousel-indicators">
              <li data-target="#slideshowExemplo" data-slide-to="0" class="active"></li>
              <li data-target="#slideshowExemplo" data-slide-to="1"></li>
              <li data-target="#slideshowExemplo" data-slide-to="2"></li>
              <li data-target="#slideshowExemplo" data-slide-to="3"></li>
            </ol>

            <div class="carousel-inner">
              <div class="carousel-item active">
                  <img src="assets/images/foto1.png" class="w-100" alt="">
              </div>
              <div class="carousel-item">
                  <img src="assets/images/foto2.png" class="w-100" alt="">
              </div>
              <div class="carousel-item">
                  <img src="assets/images/foto3.png" class="w-100" alt="">
              </div>
              <div class="carousel-item">
                  <img src="assets/images/foto4.png" class="w-100" alt="">
              </div>
            
            </div>
            <a class="carousel-control-prev" href="#slideshowExemplo" data-slide="prev"><span class="carousel-control-prev-icon"></span></a>
            <a class="carousel-control-next" href="#slideshowExemplo" data-slide="next"><span class="carousel-control-next-icon"></span></a>

        </div>
        
     </div> 

    <script src="assets/js/jquery-3.4.1.min.js"> </script>
    <script src="assets/js/bootstrap.bundle.min.js" ></script>
  </body>
</html>