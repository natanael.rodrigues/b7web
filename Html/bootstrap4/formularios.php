<!doctype html>
<html lang="pt-br">
  <head>
    <title>Portal Modelo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" >
   
  </head>
  <body>
     <div class="container">

        <form method="POST">
          E-Mail: <br><br>
          <input type="email" name="email" /><br><br>
          Senha: <br><br>
          <input type="password" name="senha" > <br><br>
          <input type="button" value="Enviar"/>
        </form>   

        <hr> <br><br>


        Primeiro caso bootstrap <br><br>

        <form method="POST">
          <div class="form-group">
            <label for="email">E-Mail:</label>
            <input type="email" name="email" class="form-control" id="email"/>
          </div>
          <div class="form-group">
            <label for="senha">Senha:</label>
            <input type="password" name="senha" class="form-control" id="senha"/> 
          </div>
          <div class="form-group">
            <input class="btn btn-primary" type="button" value="Enviar">
          </div>
        </form>   


        <hr> <br><br>


        Segundo caso bootstrap <br><br>

        <form method="POST">
          <div class="form-group">
            <input type="email" name="email" class="form-control" placeholder="Informe o Email"/>
          </div>
          <div class="form-group">
            <input type="password" name="senha" class="form-control" placeholder="Informe a senha"/> 
          </div>
          <div class="form-group">
            <input class="btn btn-primary" type="button" value="Enviar">
          </div>
        </form>  


        <hr> <br><br>


        Terceiro caso bootstrap <br><br>

        <form method="POST">
          <div class="form-group">
            <label for="email">E-Mail:</label>
            <input type="email" name="email" class="form-control form-control-lg" id="email"/>
          </div>
          <div class="form-group">
            <label for="senha">Senha:</label>
            <input type="password" name="senha" class="form-control form-control-sm" id="senha"/> 
          </div>
          <div class="form-group">
            <input class="btn btn-primary" type="button" value="Enviar">
          </div>
        </form>   


        <hr> <br><br>


        Quarto caso bootstrap <br><br>

        <form method="POST">
          <div class="form-group">
            <label for="Nome">E-Mail:</label>
            <input type="text" name="nome" class="form-control-plaintext" id="nome" value="Natanael Roberto Rodrigues" readonly/>
          </div>
          <div class="form-group">
            <label for="email">E-Mail:</label>
            <input type="email" name="email" class="form-control" id="email" value="natanaelrodrigues@gmail.com" readonly/>
          </div>
          <div class="form-group">
            <label for="senha">Senha:</label>
            <input type="password" name="senha" class="form-control" id="senha"/> 
          </div>
          <div class="form-group">
            <input class="btn btn-primary" type="button" value="Enviar">
          </div>
        </form>   

        <hr> <br><br>

        Quinto caso bootstrap <br><br>

        <form method="POST" class="form-inline">
          <div class="form-group">
            <input type="email" name="email" class="form-control" placeholder="Informe o Email"/>
          </div>
          <div class="form-group">
            <input type="password" name="senha" class="form-control" placeholder="Informe a senha"/> 
          </div>
          <div class="form-group">
            <input class="btn btn-primary" type="button" value="Enviar">
          </div>
        </form>  


        <hr> <br><br>

        Sexto caso bootstrap <br><br>

        <form method="POST">
          <div class="form-row">
            <div class="col">
                <div class="form-group">
                  <input type="text" name="name" class="form-control" placeholder="Nome Completo"/>
                </div>
            </div>
            <div class="col">
              <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Informe o Email"/>
              </div>
            </div>
            <div class="col">
              <div class="form-group">
                <input type="password" name="senha" class="form-control" placeholder="Informe a senha"/> 
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group">
              <input class="btn btn-primary" type="button" value="Enviar">
            </div>
          </div>
        </form> 
        
     </div> 

    <script src="assets/js/jquery-3.4.1.min.js"> </script>
    <script src="assets/js/bootstrap.bundle.min.js" ></script>
  </body>
</html>