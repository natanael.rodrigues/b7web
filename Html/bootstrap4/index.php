<!doctype html>
<html lang="pt-br">
  <head>
    <title>Portal Modelo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
      .row[class^⁼col-], .row .col {
        background-color : #DDD;
        border: 1px solid #CCC;
      }

      .avatar{
        width: 50px;
        height: 50px;
      }

    </style>
  </head>
  <body>
     <div class="container">
        <div class="row">
        
          <div class="col-2">...</div>
          <div class="col-2">...</div>
          <div class="col">...</div>
        </div>
        <div class="row">
          <!-- <div class="w-100"></div> Quebra linha -->
          <div class="col">...</div>
          <div class="col">...</div>
          <div class="col">...</div>
        </div>
        <hr>
          <h1>Lista de Media</h1>
          <ul class="list-unstylied">
            <li class="media">
              <img src="assets/images/me.jpeg" class="mr-3 avatar align-self-center">
              <div class="media-body">
                <h5>Titulo do assunto discutido</h5>
                <p>Lorem ipsum dolor sit amet consectetur adipiscing elit posuere imperdiet erat leo pellentesque, rhoncus habitant suscipit vel torquent mauris fusce scelerisque integer consequat. Tempus fames pellentesque dis vestibulum lorem ullamcorper maximus vehicula quisque nascetur facilisis imperdiet risus vel mi ac, tortor ultrices inceptos habitant commodo enim mauris euismod vulputate at himenaeos nisl non dignissim. Sapien ac venenatis vulputate hac blandit pretium ex aliquet iaculis mattis massa, egestas dapibus sociosqu lacinia integer odio himenaeos id enim tristique, habitasse condimentum amet facilisi aenean adipiscing efficitur habitant phasellus euismod. Ante suspendisse vitae leo molestie felis tincidunt lorem, dignissim risus mus odio fermentum sem maecenas sed, imperdiet nascetur nullam integer porttitor tristique. Mauris sagittis auctor tristique ultricies bibendum eleifend fermentum, donec malesuada habitasse sem primis turpis vitae ipsum, libero proin integer class pharetra aenean. Dignissim fusce sapien purus praesent rutrum orci donec mi eu interdum elit lacus ultricies commodo, sagittis ante condimentum curabitur massa mollis inceptos mauris porttitor faucibus efficitur quis dolor.</p>

              </div>
            </li>
            <li class="media">
              <img src="assets/images/me.jpeg" class="mr-3 avatar">
              <div class="media-body">
                <h5>Titulo do assunto discutido</h5>
                <p>Lorem ipsum dolor sit amet consectetur adipiscing elit posuere imperdiet erat leo pellentesque, rhoncus habitant suscipit vel torquent mauris fusce scelerisque integer consequat. Tempus fames pellentesque dis vestibulum lorem ullamcorper maximus vehicula quisque nascetur facilisis imperdiet risus vel mi ac, tortor ultrices inceptos habitant commodo enim mauris euismod vulputate at himenaeos nisl non dignissim. Sapien ac venenatis vulputate hac blandit pretium ex aliquet iaculis mattis massa, egestas dapibus sociosqu lacinia integer odio himenaeos id enim tristique, habitasse condimentum amet facilisi aenean adipiscing efficitur habitant phasellus euismod. Ante suspendisse vitae leo molestie felis tincidunt lorem, dignissim risus mus odio fermentum sem maecenas sed, imperdiet nascetur nullam integer porttitor tristique. Mauris sagittis auctor tristique ultricies bibendum eleifend fermentum, donec malesuada habitasse sem primis turpis vitae ipsum, libero proin integer class pharetra aenean. Dignissim fusce sapien purus praesent rutrum orci donec mi eu interdum elit lacus ultricies commodo, sagittis ante condimentum curabitur massa mollis inceptos mauris porttitor faucibus efficitur quis dolor.</p>

              </div>
            </li>
            <li class="media">
              <img src="assets/images/me.jpeg" class="mr-3 avatar">
              <div class="media-body">
                <h5>Titulo do assunto discutido</h5>
                <p>Lorem ipsum dolor sit amet consectetur adipiscing elit posuere imperdiet erat leo pellentesque, rhoncus habitant suscipit vel torquent mauris fusce scelerisque integer consequat. Tempus fames pellentesque dis vestibulum lorem ullamcorper maximus vehicula quisque nascetur facilisis imperdiet risus vel mi ac, tortor ultrices inceptos habitant commodo enim mauris euismod vulputate at himenaeos nisl non dignissim. Sapien ac venenatis vulputate hac blandit pretium ex aliquet iaculis mattis massa, egestas dapibus sociosqu lacinia integer odio himenaeos id enim tristique, habitasse condimentum amet facilisi aenean adipiscing efficitur habitant phasellus euismod. Ante suspendisse vitae leo molestie felis tincidunt lorem, dignissim risus mus odio fermentum sem maecenas sed, imperdiet nascetur nullam integer porttitor tristique. Mauris sagittis auctor tristique ultricies bibendum eleifend fermentum, donec malesuada habitasse sem primis turpis vitae ipsum, libero proin integer class pharetra aenean. Dignissim fusce sapien purus praesent rutrum orci donec mi eu interdum elit lacus ultricies commodo, sagittis ante condimentum curabitur massa mollis inceptos mauris porttitor faucibus efficitur quis dolor.</p>

              </div>
            </li>
            <li class="media">
              <img src="assets/images/me.jpeg" class="mr-3 avatar">
              <div class="media-body">
                <h5>Titulo do assunto discutido</h5>
                <p>Lorem ipsum dolor sit amet consectetur adipiscing elit posuere imperdiet erat leo pellentesque, rhoncus habitant suscipit vel torquent mauris fusce scelerisque integer consequat. Tempus fames pellentesque dis vestibulum lorem ullamcorper maximus vehicula quisque nascetur facilisis imperdiet risus vel mi ac, tortor ultrices inceptos habitant commodo enim mauris euismod vulputate at himenaeos nisl non dignissim. Sapien ac venenatis vulputate hac blandit pretium ex aliquet iaculis mattis massa, egestas dapibus sociosqu lacinia integer odio himenaeos id enim tristique, habitasse condimentum amet facilisi aenean adipiscing efficitur habitant phasellus euismod. Ante suspendisse vitae leo molestie felis tincidunt lorem, dignissim risus mus odio fermentum sem maecenas sed, imperdiet nascetur nullam integer porttitor tristique. Mauris sagittis auctor tristique ultricies bibendum eleifend fermentum, donec malesuada habitasse sem primis turpis vitae ipsum, libero proin integer class pharetra aenean. Dignissim fusce sapien purus praesent rutrum orci donec mi eu interdum elit lacus ultricies commodo, sagittis ante condimentum curabitur massa mollis inceptos mauris porttitor faucibus efficitur quis dolor.</p>

              </div>
            </li>
          </ul>
        <hr>
        <h1>Recurso Media</h1>
        <div class="media">
          <img src="assets/images/me.jpeg" class="mr-3 avatar">
          <div class="media-body">
            <h5>Titulo do assunto discutido</h5>
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit posuere imperdiet erat leo pellentesque, rhoncus habitant suscipit vel torquent mauris fusce scelerisque integer consequat. Tempus fames pellentesque dis vestibulum lorem ullamcorper maximus vehicula quisque nascetur facilisis imperdiet risus vel mi ac, tortor ultrices inceptos habitant commodo enim mauris euismod vulputate at himenaeos nisl non dignissim. Sapien ac venenatis vulputate hac blandit pretium ex aliquet iaculis mattis massa, egestas dapibus sociosqu lacinia integer odio himenaeos id enim tristique, habitasse condimentum amet facilisi aenean adipiscing efficitur habitant phasellus euismod. Ante suspendisse vitae leo molestie felis tincidunt lorem, dignissim risus mus odio fermentum sem maecenas sed, imperdiet nascetur nullam integer porttitor tristique. Mauris sagittis auctor tristique ultricies bibendum eleifend fermentum, donec malesuada habitasse sem primis turpis vitae ipsum, libero proin integer class pharetra aenean. Dignissim fusce sapien purus praesent rutrum orci donec mi eu interdum elit lacus ultricies commodo, sagittis ante condimentum curabitur massa mollis inceptos mauris porttitor faucibus efficitur quis dolor.</p>

          </div>
        </div>
        
     </div> 

    <script src="assets/js/jquery-3.4.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="assets/js/bootstrap.bundle.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>