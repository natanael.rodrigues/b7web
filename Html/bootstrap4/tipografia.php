<!doctype html>
<html lang="pt-br">
  <head>
    <title>Portal Modelo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style type="text/css">
       .teste{
         width:300px;
         height: 300px;
         border: 1px solid #000;
       }

       #quadro{
         width:550px;
         height: 550px;
         border: 1px solid #000;
       }
    </style>
  </head>
  <body>
     <div class="container ">
        <div class="h1">Titulo secundário em destaque...</div>
        <h3>Cadastro de Usuários <small class="text-muted">[Adicionar usuários]</small> </h3>
        <p>Lorem ipsum dolor sit <spam class="mark">amet consectetur adipiscing elit posuere imperdiet</spam> erat leo pellentesque, rhoncus habitant suscipit vel torquent mauris fusce scelerisque integer consequat. Tempus fames pellentesque dis vestibulum lorem ullamcorper maximus vehicula quisque nascetur facilisis imperdiet risus vel mi ac, tortor ultrices inceptos habitant commodo enim mauris euismod vulputate at himenaeos nisl non dignissim. Sapien ac venenatis vulputate hac blandit pretium ex aliquet iaculis mattis massa, egestas dapibus sociosqu lacinia integer odio himenaeos id enim tristique, habitasse condimentum amet facilisi aenean adipiscing efficitur habitant phasellus euismod. Ante suspendisse vitae leo molestie felis tincidunt lorem, dignissim risus mus odio fermentum sem maecenas sed, imperdiet nascetur nullam integer porttitor tristique. Mauris sagittis auctor tristique ultricies bibendum eleifend fermentum, donec malesuada habitasse sem primis turpis vitae ipsum, libero proin integer class pharetra aenean. Dignissim fusce sapien purus praesent rutrum orci donec mi eu interdum elit lacus ultricies commodo, sagittis ante condimentum curabitur massa mollis inceptos mauris porttitor faucibus efficitur quis dolor.</p>
        <div class='container d-flex justify-content-center' id="quadro"> <div class="teste align-self-center text-center">Algum texto de teste</div></div>
        

        <div class="list-inline">
          <li class="list-inline-item">Item 1</li>
          <li class="list-inline-item">Item 2</li>
          <li class="list-inline-item">Item 3</li>
          <li class="list-inline-item">Item 4</li>
        </ul>
     </div> 

    <script src="assets/js/jquery-3.4.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="assets/js/bootstrap.bundle.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>