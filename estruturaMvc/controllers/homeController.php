<?php 

    class homeController extends controller{
        public function index(){

            $anuncios = new anuncios();

            $dados  = array(
                'quantidade' => $anuncios->getQuantidade()
            );
            
            $this->loadTemplate('home', $dados);
        }
    }