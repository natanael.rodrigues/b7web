<!doctype html>
<html lang="pt-br">
  <head>
    <title>Mdelo MVC</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
      
    <?php $this->loadViewInTemplate($viewName, $viewData); ?>


    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery-3.4.1.min.js" ></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap.bundle.min.js" ></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/script.js"></script>

  </body>
</html>