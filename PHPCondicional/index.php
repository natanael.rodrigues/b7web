<?php

    $nome = 'natanael';


    // CONDICIONAL BASICO IF
    if ($nome == 'natanael') {
        echo "Acertou <br/>";
    };

    //CONDICIONAL TERNÁRIO

    $resultado = ($nome == 'natanael') ? "ACERTOU NOVAMENTE <br/>" : "NÃO ACERTOU<br/>"; 

    echo $resultado;

    // CONDICIONAL NULL CAO
    $nomeCompleto = $nome;
    $nomeCompleto .= $sobrenome ?? ''; // essa condição é o null cao.
    //  a sentença acima subistitui a linha abaixo
    //$nomeCompleto .= $sobrenome ? $sobrenome : '';

    echo $nomeCompleto;
