<?php   
    class Pessoa {

        private $nome;
        
        public static function getInstance(){

            static $instance = null;
            if ($instance === null){
                $instance = new Pessoa();
            }

            return $instance;
        }

        private function __construct(){
            
        }

        public function getNome(){
            return $this->nome;
        }

        public function setNome($nome){
            $this->nome = $nome;
        }
    }


    // instanciar uma classe singleton
    $cara = Pessoa::getInstance();