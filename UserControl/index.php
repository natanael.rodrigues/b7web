<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User Control</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <?php require 'config.php' ?>

    <div class="container">
        <a href="adicionar.php">Adicionar Novo Usuário </a>
        <table class="table">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>E-Mail</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $sql = "SELECT * FROM USUARIOS";
                    $sql = $pdo->query($sql);

                    if ($sql->rowCount() > 0){
                        foreach($sql->fetchAll() as $usuario) {
                            echo '<tr>';
                            echo '   <td scope="row">'. $usuario['nome']. '</td>';
                            echo '    <td>'.$usuario['email'] .'</td>';
                            echo '    <td><a href="editar.php?id=' . $usuario['id'] . '">Editar</a> - <a href="excluir.php?id=' . $usuario['id'] . '"">Excluir</a> </td>';
                            echo '</tr>';
                        }
                    }
                ?>


            </tbody>
        </table>
    </div>

    <script type="text/javascript" src="assets/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
</body>
</html>