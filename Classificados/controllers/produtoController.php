<?php
    class ProdutoController extends controller{
        public function index(){

        }

        public function abrir($id){

            $a = new Anuncios();
            $u = new Usuarios();

            $dados = array();
            
            if (empty($id)){
                header("Location:".BASE_URL);
                exit;
            }
            
            $info = $a->getAnuncio($id);

            $dados['info'] = $info; 

            $this->loadTemplate('produto', $dados);

        }
    }