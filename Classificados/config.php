<?php
    require 'environment.php';

    $config = array();

    if (ENVIRONMENT == 'development'){
        define("BASE_URL","http://localhost/b7web/Classificados/");
        $config['dbname'] = 'classificados';
        $config['host'] = 'localhost';
        $config['dbUser'] = 'root';
        $config['dbPass'] = '';
    } else {
        define("BASE_URL","./");
        $config['dbname'] = '';
        $config['host'] = '';
        $config['dbUser'] = '';
        $config['dbPass'] = '';
    }

    global $db;
    try{
        $db = new PDO("mysql:dbname=".$config['dbname'].";host=".$config['host'],$config['dbUser'],$config['dbPass']);
        
    } catch(PDOException $e){
        echo "ERRO: ".$e->getMessage();
        exit;
    }