function apertouMouse(){
    console.log("Apertou o mouse");
}

function soltouMouse(){
    console.log("Soltou o mouse");
}

function mouseEmCima(){
    console.log("mouse passou por cima");
}

function MouseSaiudeCima(){
    console.log("Mouse saiu de cima.");
}

function moveuMouse(){
    console.log("Moveu mouse");
}

function clicou(){
    console.log("Clicou...")
}

function botaoDireito(){
    console.log("Botão direito");
    // cancela o menu
    return false;
}

function duploClick(){
    console.log("Duplo click...")
}

function apertouTeclado(event){
    console.log("apertou alguma tecla...");
    console.log(event.keyCode);
}

function soltouTecla(event){
    console.log("Soltou a tecla");
}

function pressionouTeclado(event){
    // shiftKey - shift
    // ctrlKey - Control
    // altKay - Alt
    event.ctrl
    if ( event.shiftKey == true){
        console.log("Apertou uma tecla com o shift ativo");
    } else {
        console.log("pressinou teclado...");
    }
}

function carregouPagina(){
    console.log("Carregou a pagina...")
}

function fechouPagina(){
    alert("Fechando a pagina...");
}

function mudouOpcao(objeto){
    console.log("selecionou a cidade " + objeto.value)
}

function focou(){
    console.log("Setou foco...")
}

function desfocou(){
    console.log("Saiu do campo...")
}