/*
  RECEBE DATA
  dt = new Date();

  pegar hora
  dt.getHours();

  pegar quantidade de minutos
  dt.getMinutes();

  pegar segundos
  dt.getSeconds();

  setar uma data
  dt = new Date(Date.parse("March 10, 2001"));

  pegar dia
  dt.getDate();

  pegar dia da semana
  dt.getDay();

  pegar ano
  dt.getFullYear();

  pegar mes
  dt.getMonth(); começa com zero = janeiro

    pegar milissegundos atuais
    dt.getMilliseconds();

    pegar total de milisegundos da data
    dt.getTime();
*/