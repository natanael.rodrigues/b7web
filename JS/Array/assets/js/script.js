let lista = ['Banana','Laranja','Tomate','Azul','Rosa']

let res = lista.toString();

console.log('Array para string:',res);


res = lista.join(';');

console.log('Array para string identificando o agrupador:',res);

res = lista.indexOf('Azul');

console.log('Pega a posição no array através do indexOf():',res);

console.log(lista);

lista.pop();

res = lista;

console.log('Retiro o ultimo registro da lista...:',res);

lista.shift();

res = lista;

console.log('Retiro o primeiro registro da lista...:',res);


lista.push('Novo registro');

res = lista;

console.log('Inclui novo registro na lista...:',res);

lista[1] = "Item alterado";

res = lista;

console.log('Altera o registro na lista...:',res);

lista.splice(1,1); // primeiro parâmetro é a posição, segundo é a quantidade.

res = lista;

console.log('Remove o registro na lista...:',res);

let lista2 = ['Joinville','Itajai','Blumenau']

lista.concat(lista2);

res = lista.concat(lista2);

console.log('Concatenando array...:',res);


lista.sort(); // primeiro parâmetro é a posição, segundo é a quantidade.

res = lista;

console.log('Lista ordenada em ordem alfabetica...:',res);

lista.reverse(); // primeiro parâmetro é a posição, segundo é a quantidade.

res = lista;

console.log('Lista ordenada em ordem inversa...:',res);

lista = [45, 4, 9, 16, 25];

lista2 = [];

lista2 = lista.map((item) => {
    return item * 2;
});
console.log('Novo array...:',lista);
console.log('Varrendo um array e executando uma determinava função MAP (mapeando um array)...:',lista2);


lista2 = [];

lista2 = lista.filter((item) => {
    return item < 20;
});
console.log('Novo array...:',lista);
console.log('Varrendo um array e executando uma determinava função (FILTER - filtrando um array)...:',lista2);

lista2 = [];

lista2 = lista.every((item) => {
    return item > 20;
});
console.log('Novo array...:',lista);
console.log('Varrendo um array e executando uma determinava função (every - verifica se todos os itens pertencem a uma regra)...:',lista2);

lista2 = [];

lista2 = lista.find((item) => {
    return item == 16;
});
console.log('Novo array...:',lista);
console.log('Buscando um item no array...:',lista2);

lista2 = [];

lista2 = lista.findIndex((item) => {
    return item == 16;
});
console.log('Novo array...:',lista);
console.log('Buscando um item no array e retornando o index dele...:',lista2);



let info = ["Natanael Roberto",'Natanael','Rodrigues','@natanaelrodrigues'];

let [nomeCompleto, nome, sobrenome, instagram] = info;

console.log('Desconstruindo o array: ' + info);
console.log('Nome ' + nome);
console.log('Sobrenome ' + sobrenome);
console.log('nome completo ' + nomeCompleto);
console.log('instagram ' + instagram);

console.log('+++++++++++++++++++++++')
