//Valores iniciais
let areas = {
  a: null,
  b: null,
  c: null,
};
//Eventos
document.querySelector(".neutralArea").addEventListener("click", (e) => {});

document.querySelectorAll(".item").forEach((item) => {
  item.addEventListener("dragstart", dragStart);
  item.addEventListener("dragend", dragEnd);
});

document.querySelectorAll(".area").forEach((area) => {
  area.addEventListener("dragover", dragOver);
  area.addEventListener("dragleave", dragLeave);
  area.addEventListener("drop", Drop);
});

document
  .querySelector(".neutralArea")
  .addEventListener("dragover", dragOverNeutral);
document
  .querySelector(".neutralArea")
  .addEventListener("dragleave", dragLeaveNeutral);
document.querySelector(".neutralArea").addEventListener("drop", DropNeutral);

//functions item

function dragStart(e) {
  e.currentTarget.classList.add("dragging");
}

function dragEnd(e) {
  e.currentTarget.classList.remove("dragging");
}

//functions area
function dragOver(e) {
  // PASSOU POR CIMA
  if (e.currentTarget.querySelector(".item") === null) {
    // Liberando o objeto
    e.preventDefault();

    e.currentTarget.classList.add("hover");
  }
}

function dragLeave(e) {
  // SAIU DA AREA

  e.currentTarget.classList.remove("hover");
}

function Drop(e) {
  // SOLTOU O OBJETO
  e.currentTarget.classList.remove("hover");

  if (e.currentTarget.querySelector(".item") === null) {
    let dragItem = document.querySelector(".item.dragging");
    e.currentTarget.appendChild(dragItem);
    updateAreas();
  }
}

function dragOverNeutral(e) {
  e.preventDefault();

  e.currentTarget.classList.add("hover");
}

function dragLeaveNeutral(e) {
  e.currentTarget.classList.remove("hover");
}

function DropNeutral(e) {
  e.currentTarget.classList.remove("hover");
  let dragItem = document.querySelector(".item.dragging");
  e.currentTarget.appendChild(dragItem);
  updateAreas();
}

//  REGRA DE NEGÓCIO...

function updateAreas() {
  document.querySelectorAll(".area").forEach((area) => {
    let name = area.getAttribute("data-name");

    if (area.querySelector(".item") !== null) {
      areas[name] = area.querySelector(".item").innerHTML;
    } else {
      areas[name] = null;
    }
  });

  if (areas.a === "1" && areas.b === "2" && areas.c === "3") {
    document.querySelector(".areas").classList.add("correct");
  } else {
    document.querySelector(".areas").classList.remove("correct");
  }
}
