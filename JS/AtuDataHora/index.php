<!doctype html>
<html lang="pt-br">
  <head>
    <title>Portal Modelo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
  </head>
  <body>
     <div class="container">
        <label for="data">
          Data:<br>
          <input type="date" name="ddata" id="ddata">
        </label>
        <label for="dhora">
          Hora:<br>
          <input type="time" name="dtime" id="dtime">
        </label>
     </div> 
     <script src="assets/js/jquery-3.4.1.min.js" ></script>
     <script src="assets/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
     <script src="assets/js/all.min.js" ></script>

     <script>
      
        let fieldData = document.getElementById('ddata');// $("ddata").html("teste");
        fieldData.value = "2021-03-04";
     
        let fieldTime = document.getElementById('dtime');// $("ddata").html("teste");
     
        fieldTime.value = '20:45';
     </script>
  </body>

</html>