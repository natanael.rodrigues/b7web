d = new Date();

let res = d.getFullYear();

console.log('Ano', res);

res = d.getMonth();

console.log('Mes', res + 1); // vai vir com menos um.

res = d.getDay();

console.log('Dia', res);

res = d.getDay();

console.log('Dia da semana', res);

res = d.getDate();

console.log('Dia do mes', res);

res = d.getHours()

console.log('Hora', res);

res = d.getMinutes()

console.log('Minutos', res);

res = d.getSeconds()

console.log('Segundos', res);

res = d.getMilliseconds()

console.log('Millisegundos', res);

res = d.getTime()

console.log('Data em Timestamp', res);

d.setFullYear( d.getFullYear() + 10);
res = d;

console.log('Acrescentando anos', res.getFullYear());