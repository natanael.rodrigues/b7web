// dados iniciais
let currentColor = "black";
let screen = document.querySelector("#tela");
let ctx = screen.getContext("2d");
let canDraw = false;
let mouseX;
let mouseY;

// eventos
document.querySelectorAll(".colorArea .color").forEach((item) => {
  item.addEventListener("click", colorClickEvent);
});

screen.addEventListener("mousedown", mouseDownEvent);
screen.addEventListener("mousemove", mouseMoveEvent);
screen.addEventListener("mouseup", mouseUpEvent);

document.querySelector(".clear").addEventListener(click, clearScreen);

// funções
function colorClickEvent(e) {
  let color = e.target.getAttribute("data-color");
  currentColor = color;

  document.querySelector(".color.active").classList.remove("active");
  e.target.classList.add("active");
}

function mouseDownEvent(e) {
  canDraw = true;
  mouseX = e.pageX - screen.offsetLeft;
  mouseY = e.pageY - screen.offsetTop;
}

function mouseMoveEvent(e) {
  if (canDraw) {
    let pointX = e.pageX - screen.offsetLeft;
    let pointY = e.pageY - screen.offsetTop;
    draw(pointX, pointY);
  }
}

function mouseUpEvent() {
  canDraw = false;
}

function draw(x, y) {
  ctx.beginPath();
  ctx.lineWidth = 5;
  ctx.lineJoin = "round";
  ctx.moveTo(mouseX, mouseY);
  ctx.lineTo(x, y);
  ctx.closePath();

  ctx.strokeStyle = currentColor;
  ctx.stroke();

  mouseX = x;
  mouseY = y;
}

function clearScreen() {
  ctx.setTransform(1, 0, 0, 1, 0, 0);
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}
