<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Portal Modelo</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <div class="container">
        <input type="text" name="cep" id="cep">
    </div>

    <script type="text/javascript" src="assets/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.mask.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>
</html>

