function acao(){
    document.write("Executou... <br>");
}
// executa de tempo em tempo como parametrizado.
//setInterval(acao,2000)

// espera um tempo(parametrizado) e executa a funçao     
setTimeout(acao,2000);

// definindo o setInterval em uma variável quando usar a função clearInterval(); ele encerra
// var timer = setInterval(acao,2000);
//clearInterval(timer);