lgpdUrl = "https://jsonplaceholder.typicode.com/posts";
let lgpd_html = `
    <div class="lgpd">
        <div class="lgpd--left">
            Nós utilizamos cookie para melhorar sua experiência do usuário <br>
            Para conferir detalhadamente todos os cookies utilizados leia nossa <a href="#">política de privacidade</a>
        </div>
        <div class="lgpd--right">
            <button>OK</button>
        </div>
    </div>

    <link rel="stylesheet" href="style.css">
`;

let lsContent = localStorage.getItem("lgpd");

if (!lsContent) {
  document.body.innerHTML += lgpd_html;

  lgpd_area = document.querySelector(".lgpd");
  let lgpd_button = lgpd_area.querySelector("button");

  lgpd_button.addEventListener("click", async () => {
    lgpd_area.remove();

    let result = await fetch(lgpdUrl);
    let json = await result.json();

    if (json.error != "") {
      let id = "123"; // json.id
      localStorage.setItem("lgpd", id);
    }
  });
}
