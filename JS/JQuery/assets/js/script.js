$(function(){

    // muda o atributo
    $('a').attr('href','http://www.google.com.br');
    $('img').attr('width','300');
    
    
    // buscar um atributo de um objeto
    //link = $('a').attr('href');

   // console.log(link);

   //selecionar uma div
   $('#teste').html("Texto mudado");

   //ver o conteudo de uma tributo
   //$('#teste').html();

   // .addClass = Adicionar uma classe em um estilo

   $('#tougle').click(function(){
       $(this).toggleClass('fundoazul');
   });

   $('#limon').mouseover(function(){
       $(this).addClass('fundoamarelo');
   });


   $('#limon').mouseout(function(){
     $(this).removeClass('fundoamarelo');
    });

    // os dois passos acima em um unico evento. // HOVER
    $('#limon-hover').hover(function(){
        $(this).addClass('fundoamarelo');
    },
    function(){
        $(this).removeClass('fundoamarelo');
    });

    $('#tougle-hover').hover(function(){
        $(this).toggleClass('fundoazul');
    });


    $('#form').bind('submit', function(e){
        e.preventDefault();

        console.log('Formulado submitado!')
    });


    $('#nome').bind('select', function(){

        console.log('Algo foi selecionado!')
    });

    
    $('#nome, #email, #idade').bind('focus', function(){

        $(this).toggleClass('focado');
    });

    $('#nome, #email, #idade').bind('blur', function(){
        $(this).toggleClass('focado');
    });


    // eventos de teclado.
    $('#nome').bind('keydown', function(){
        console.log("Apertou");
    });

    $('#nome').bind('keyup', function(e){
        console.log("Soltou! Tecla apertada...: codigo " + e.keyCode);
        if (e.keyCode == 13) {
            console.log($(this).val());
            $(this).val('');

        };
        
    });


    // fim eventos de teclado.
    
    $('#idade').bind('change', function(){
        console.log($(this).val());
    });

    // evento do mouse

    $('.botao').bind('mousedown',function(){
        console.log('Apertou o botão do mouse.');
    });

    $('.botao').bind('mouseup',function(){
        console.log('liberou o botão do mouse.');
    });

    $('.botao').bind('mousemove',function(){
        console.log('moveu o mouse.');
    });

    $('.botao').bind('mouseover',function(){
        console.log('moveu está em cima.');
        $(this).addClass('botao_emcima');
    });

    
    $('.botao').bind('mouseout',function(){
        console.log('mouse saiu.');
        $(this).removeClass('botao_emcima');
    });


    $('.botao').bind('dblclick',function(){
        console.log('Clicou duas vezes.');
    });

     // FIM DOS eventos do mouse
});