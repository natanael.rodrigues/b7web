let timer;
let timeOut;

setTimeout(function(){
    alert("Seja bem Vindo!");
},5000);


function showTime(){
    let d = new Date();

    let h = d.getHours();
    let m = d.getMinutes();
    let s = d.getSeconds();

    let txt = h + ':' + m + ':' + s;

    document.querySelector('.demo').innerHTML = txt;

};

function comecar(){
    console.log('Começando o Timer');
    timer = setInterval(showTime, 1000);
};

function parar(){
    console.log('Parando o Timer.');

    clearInterval(timer);
};

function rodar(){
    timeOut = setTimeout(function(){
        alert("Seja bem Vindo!");
    },5000);
};

function cancelar(){
    clearTimeout(timeOut);
};