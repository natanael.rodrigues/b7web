$(function(){
    $('#botao').bind('click', function(){
        $('.grupo_botao').toggle('slow'); // ou fast
    });

    $('#fade').bind('click', function(){
        $('.grupo_botao').fadeToggle('slow'); // .fadeIn(); .fadeOut(); .fadeTo('slow',0,5); 0 a 1
    });

    $('#slide').bind('click', function(){
        $('.grupo_botao').slideToggle('slow'); // .slideUP(); .slideDown();
    });

    $('.hellobar').bind('click', function(){
        $(this).slideUp();
    });
    
    $('#hellobar').bind('click', function(){
        $('.hellobar').slideToggle();
    });


    $('#animar').bind('click', function(){
        $('.grupo_botao').animate({
            'margin-left': '100px',
            'margin-top': '20px',
            'width':'500px',
            'border-radius':75
        },1000);
    });

    $('#animar2').bind('click', function(){
        $('.grupo_botao').animate({
            'margin-left': '100px',
            'margin-top': '20px',
            'width':'500px',
            'border-radius':75
        },{
            durtion: 1500,
            complete: function(){
                console.log('Animação finalizada.');
            },
            start: function(){
                console.log('Começou a animação.');
            },
            step: function(){
                // será chamada a cada etapa da animação.
                console.log('Nova etapa...');
            }
        });
    });


    $('#parar').bind('click', function(){
        $('.grupo_botao').stop();
    });

    $('#somar').bind('click', function(){
        $('.grupo_botao').animate({
            'margin-left': '+=50'
        },1000);
    });
   
});