<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>JQuery...</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    
</head>
<body>
    <div class="container">
        <div class="hellobar">.</div>
        <div class="botao" id="botao">Botão</div>
        
        <div class="grupo_botao"></div>
        <div class="botao" id='fade'>Botão fade</div>
        <div class="botao" id='slide'>Botão Slide</div>
        <div class="botao" id='hellobar'>Brinca com Hellobar</div>
        <div class="botao" id='animar'>Animação</div>
        <div class="botao" id='animar2'>Animação</div>
        <div class="botao" id='parar'>Para Animação</div>
        <div class="botao" id='somar'>Somar Animação</div>
        
    </div>







    <script type="text/javascript" src="assets/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>
</html>


