// ouve o clicar do teclado.
document.body.addEventListener('keyup',(event)=>{
    // executa o som da tecla.
    playSound(event.code.toLowerCase());
});

document.querySelector('.composer button').addEventListener('click',()=>{
    let song = document.querySelector('#input').value;

    if(song !== ''){
        let songArray = song.split('');

        playComposition(songArray);
    }

});

function playSound(sound){
    let audioElement = document.querySelector(`#s_${sound}`);
    let keyElement = document.querySelector(`div[data-key="${sound}"]`);

    // executa o audio.
    if(audioElement){
        audioElement.currentTime = 0;
        audioElement.play();
    }

    // assinala qual tecla está setando.
    if (keyElement){
        keyElement.classList.add('active');

        setTimeout(()=>{
            keyElement.classList.remove('active');
        },300);
    }
}

// roda a composição
function playComposition(composition){
    let wait = 0;
    for(let item of composition){
        setTimeout(()=>{
            playSound(`key${item}`);
        },wait);

        wait += 250;
    }
}