<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Portal Modelo</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
</head>
<body>
    <div class="menu">
        <ul>
            <li>
                <div class='menu1'>Home</div>    
                <div class='menu2'>
                    Opção 1 <Br>
                    Opção 2 <Br>
                    Opção 3 <Br>
                     
                </div>
            </li>
            <li>
                <div class='menu1'>Institucional</div>
                <div class='menu2'>
                    Opção 1 <Br>
                    Opção 2 <Br>
                    Opção 3 <Br>
                    Opção 4 <Br>
                </div>
            </li>
            <li>
                <div class='menu1'>Cadastro</div>
                <div class='menu2'>
                    Opção 1 <Br>
                    Opção 2 <Br>
                    Opção 3 <Br>
                    Opção 4 <Br>
                </div>
            </li>
            <li>
                <div class='menu1'>Consultas</div>
                <div class='menu2'>
                    Opção 1 <Br>
                    Opção 2 <Br>
                    Opção 3 <Br>
                    Opção 4 <Br>
                </div>
            </li>
            <li>
                <div class='menu1'>Utilitários</div>
                <div class='menu2'>
                    Opção 1 <Br>
                    Opção 2 <Br>
                    Opção 3 <Br>
                    Opção 4 <Br>
                </div>
            </li>
            <li>
                <div class='menu1'>Contato</div>
                <div class='menu2'>
                    Opção 1 <Br>
                    Opção 2 <Br>
                    Opção 3 <Br>
                    Opção 4 <Br>
                </div>
            </li>
        </ul>
    </div>

    <script type="text/javascript" src="assets/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.mask.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>
</html>

