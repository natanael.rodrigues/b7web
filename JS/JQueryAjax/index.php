<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>JQuery...</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    
</head>
<body>
    <div class="container">
        <div class="grupo_botao"></div>

        <button id="carregar_simples">Ação</button>
        <button id="carregar_get">Get</button>
        <button id="carregar_post">Post</button>

        <form method="POST" id="form">
            Nome: <Br>
            <input type="text" name="usuario" id="usuario"><br><br>
            Senha: <Br>
            <input type="password" name="senha" id="senha"><br><br>
            <input type="submit" value="Enviar">
        </form>

        <form method="POST" id="formnum">
            Numero 1: <Br>
            <input type="text" name="x" id="x"><br><br>
            Numero 2: <Br>
            <input type="text" name="y" id="y"><br><br>
            <input type="submit" value="Enviar">
        </form>
        <div class='resultado'></div>
        <button>Ação</button>
        <hr>

        <form method="POST" id="formJson">
            Nome: <Br>
            <input type="text" name="usuarioj" id="usuarioj"><br><br>
            Senha: <Br>
            <input type="password" name="senhaj" id="senhaj"><br><br>
            <input type="submit" value="Enviar">
        </form>
    </div>







    <script type="text/javascript" src="assets/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>
</html>


