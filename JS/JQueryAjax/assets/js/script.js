$(function(){
   $('#carregar_simples').bind('click',function(){
        $('.grupo_botao').load('teste.html');
   });

   $('#carregar_get').bind('click',function(){
        $.get('teste.html',function(t){
                $('.grupo_botao').html(t);
        });
   });

   $('#carregar_post').bind('click',function(){
        $.post('teste.html',function(t){
                $('.grupo_botao').html(t);
        });
    });

    $('#form').bind('submit',function(e){
        e.preventDefault();

        var txt = $(this).serialize(); 
        console.log(txt);       

    });

    $('#formnum').bind('submit',function(e){
        e.preventDefault();

        var txt = $(this).serialize(); 
        console.log(txt);     
        
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: txt,
            success: function(resultado){
               $('.resultado').html("Resultado: " + resultado);
            },
            error: function(){
                alert('Ocorreu um erro durante o processo.');
            }
        });

    });

    $('#formJson').bind('submit',function(e){
        e.preventDefault();

        var txt = $(this).serialize(); 
        console.log(txt);  
        $.ajax({
            type: 'POST',
            url: 'json.php',
            data: txt,
            dataType:'json',
            success: function(json){
               $('.resultado').html(json);
               alert('Nome: ' + json.nome)
            },
            error: function(){
                alert('Ocorreu um erro durante o processo.');
            }
        });
     

    });
   
});