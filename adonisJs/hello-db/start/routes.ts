import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})

/*
Route.get('/posts', 'PostsController.index');
Route.get('/posts/:id', 'PostsController.show');
Route.delete('/posts/:id', 'PostsController.destroy');
Route.post('/posts', 'PostsController.store');
Route.put('/posts/:id', 'PostsController.update');
tudo isto muda par ao item abaixo.
*/
// quando quiser criar apenas rotas de api usar o .apiOnly()
Route.resource('/posts','PostsController').apiOnly();