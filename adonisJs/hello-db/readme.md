-- Criando o projeto
npx create-adonis-ts-app hello-bd

-- Instalando o lucid
npm i @adonisjs/lucid
node ace invoke @adonisjs/lucid
--Migrations
Criar
node ace make:migration posts
Rodar
node ace migration:run
Alter table
node ace make:migration add_column_author --table=posts
remove column table
node ace make:migration del_column_author --table=posts
--Criar um Model
node ace make:model Post
