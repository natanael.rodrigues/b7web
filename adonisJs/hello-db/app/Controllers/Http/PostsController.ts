 import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
 import Post from 'App/Models/Post'

export default class PostsController {
    async index({}: HttpContextContract){
        const allPosts = await Post.all();
        
        return allPosts;
    }

    async store({request} : HttpContextContract){
        const post = request.only([
            'title',
            'content',
            'author',
        ])
        const posted = await Post.create(post);
        return posted;
    }

    async show({params, response}: HttpContextContract){
        const post_id = params.id;
        const post = await Post.find(post_id);
        if (!post) {
            response.notFound();
        }
        return post;
    }

    async destroy({params, response}: HttpContextContract){
        const {id} = params;
        const post = await Post.find(id);

        if (!post) {
            response.notFound();
        }

        await post?.delete();
    }

    async update({params ,request, response} : HttpContextContract){
        const {id} = params;
        const data = await Post.find(id);


        if (!data) {
            response.notFound();
        }

        const post = request.only([
            'title',
            'content',
            'author',
        ])

        data.merge(post);

        await data?.save();

        return data;
    }
}
