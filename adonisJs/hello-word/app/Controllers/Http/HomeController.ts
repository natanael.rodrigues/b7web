// import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class HomeController {
    async index({view}){
        let dados = {
            usuario: {
                nome: "Natanael",
                idade: 128,
                nick: 'natan',
                tecnologias: ['html','css','javascript','nodeJS']
            },

            usuarios:[
                {
                    nome: "Natanael",
                    tecnologias: ['html','css','javascript','nodeJS'],
                    admin: true
                },
                {
                    nome: 'Jose da Silva',
                    tecnologias: ['php','ts','javascript','nodeJS'],
                    admin: false
                }   
            ]
        }
        /*
        return view.render('painel.homepage',{
            param1: "Natanael Roberto Rodrigues",
            param2: 128
        });
        */

        return view.render('painel/homepage',dados);
    }

    async sobre(){
        return 'Sobre Nós';
    }
}
