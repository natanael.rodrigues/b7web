// import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class PainelsController {

    protected users = [
        {
            id:1, nick:'natan', name: 'Natanael'
        },
        {
            id: 2, 
            nick: 'rob',
            name: 'Roberto'
        }
    ]

    async index({request, response}){
        response.status(201).send({
            response: 'Index do Painel', 
            language: request.language(),
            method: request.method(),
            id: request.ip(),
            ips: request.ips(),
            qs: request.qs(),
            url: request.url(),
            completeURL: request.completeUrl(),
            imputs: request.all(),
            only: request.only(['idade']),
            except: request.except(['idade']),
            request: request
        });
    }
/*
    async usuarios(){
        return {user: this.users} ;
    }
*/
    async usuarioById({params}){
        if(!params['id']){
           return {user: this.users} ;
        } 

        let myRequestedId = params['id'];
        let myUser = this.users.find(user => user.id == myRequestedId);

        if ( myUser ) {
            return myUser;
        } else {
            return {error: 'nenhum usuario encontrato!'}
        }
               
    }

    async usuarioBySlug({params}){
        let myRequestedSlug = params['slug'];
        let myUser = this.users.find(user => user.nick == myRequestedSlug);

        if ( myUser ) {
            return myUser;
        } else {
            return {error: 'nenhum usuario encontrato!'}
        }
    }


    async admin(){
        return {response: 'Usurios do Admin'};
    }

    async docs({params}){
        return params;
    }
}
