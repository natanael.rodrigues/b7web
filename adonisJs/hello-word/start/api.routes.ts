import Route from '@ioc:Adonis/Core/Route'

Route.group(() =>{
    Route.get('/admin/','PainelController.admin');

    Route.group(() => {
        Route.get('/','PainelsController.index');
        //Route.get('/usuarios','PainelsController.usuarios');
        Route.get('/usuarios/:id?','PainelsController.usuarioById').where('id',/^[0-9]+$/);
        Route.get('/usuarios/:slug','PainelsController.usuarioBySlug').where('slug',Route.matchers.slug());

        Route.get('/docs/*','PainelController.docs');
    }).prefix('/painel/');


}).prefix('/api');
