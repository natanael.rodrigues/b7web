<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::get('/ping',function(){
    return['pong' => true];
});



/*
Route::get('/401',[AuthController::class,'unauthorized'])->name('login');

Route::post('/auth/login',[AuthController::class,'login']);
Route::post('/auth/logout',[AuthController::class,'logout']);
Route::post('/auth/refresh',[AuthController::class,'refresh']);

Route::post('/user',[UserController::class,'create']);
Route::put('/user',[UserController::class,'update']);

Route::post('/user/avatar',[UserController::class,'updateAvatar']);
Route::post('/user/cober',[UserController::class,'updateCover']);

Rote::get('/feed',[FeedController::class,'read']);
Rote::get('/user/feed',[FeedController::class,'userFeed']);
Rote::get('/user/{id}/feed}',[FeedController::class,'userFeed']);


Rote::get('/user}',[UserController::class,'read']);
Rote::get('/user/{id}',[UserController::class,'read']);

Rote::post('/feed}',[FeedController::class,'create']);

Rote::post('/post/{id}/like',[PostController::class,'like']);
Rote::post('/post/{id}/commnet',[PostController::class,'comment']);

Rote::get('/search',[SearchController::class,'search']);
*/