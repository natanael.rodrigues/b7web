
<?php

echo "Tabela Vertical. <br/><br/>";


$array = [
    'nome' => 'Natanael',
    'idade' => 90,
    'empresa' => 'Dalmark',
    'cor' => 'vermelho',
    'profissao' => 'Analista de Sistemas Senior'
];


$keys = array_keys($array);
$values = array_values($array);

echo "<table border='1'>";
foreach ($array as $key => $value):
    echo "<tr>";
    echo "<th>" . $key . "</th><td>" . $value . "</td>";
    echo "<tr/>";
endforeach;
echo "</table>";