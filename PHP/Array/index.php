<?php

echo "Função Reduce <br/><br/>";

$numeros = [ 1,2,3,4,5,6 ];

function somar($subtotal, $item){

    $subtotal += $item;

    return $subtotal;
}

$total = array_reduce($numeros, 'somar');


echo $total."<br/><br/><br/>";


echo "<h1> PESSOAS </H1> <br/><br/>";

$pessoas = [
    ['nome' => 'Fulano', 'sexo' => 'M', 'nota' => 9],
    ['nome' => 'cilcano', 'sexo' => 'M', 'nota' => 7],
    ['nome' => 'Beltrano', 'sexo' => 'M', 'nota' => 10],
    ['nome' => 'Paulo', 'sexo' => 'M', 'nota' => 8],
    ['nome' => 'Cintia', 'sexo' => 'F', 'nota' => 9],
    ['nome' => 'Jessica', 'sexo' => 'F', 'nota' => 10]
];


function contar_m($subtotal, $item){
    if ($item['sexo'] === 'M' ){
        $subtotal++;
    }
    return $subtotal;
}

$total_m = array_reduce($pessoas, 'contar_m');


echo "<br/>".$total_m."<br/><br/><br/>";