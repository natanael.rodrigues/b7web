
<?php

echo "Função Array Key  e Values. <br/><br/>";


$array = [
    'nome' => 'Natanael',
    'idade' => 90,
    'empresa' => 'Dalmark',
    'cor' => 'vermelho',
    'profissao' => 'Analista de Sistemas Senior'
];

$keys = array_keys($array);

foreach( $keys as $item){
    echo $item . "<br>";
}


echo "<br/><br/>";


$values = array_values($array);

foreach( $values as $item){
    echo $item . "<br>";
}