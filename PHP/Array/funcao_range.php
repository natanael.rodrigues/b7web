<?php

//$arrayRange = [1,2,3,4,5,6,7,8,9];
echo "Range Básico <br/>" ;
$arrayRange = range(1,100);

foreach($arrayRange as $item){
    echo $item."<br/>";
}

echo "Range Básico com pulo de itens(step)<br/>" ;
$arrayRange = range(1,100,2);

foreach($arrayRange as $item){
    echo $item."<br/>";
}