<?php

echo "Função Key Exists, se um determinado array tem uma chave.<br/><br/>";


$array = [
    'nome' => 'Natanael',
    'idade' => 90,
    'empresa' => 'Dalmark',
    'cor' => 'vermelho',
    'profissao' => 'Analista de Sistemas Senior'
];

// pode usar o array_key_exists() também

if(key_exists('idade',$array)){
    echo "<br/> Achou a chave no array";
} else {
    echo "<br/> Não achou a chave no array";
}