CREATE TABLE public.notes (
	id bigserial NOT NULL,
	title varchar(100) NOT NULL,
	body text NULL
	CONSTRAINT notes_pkey PRIMARY KEY (id)
);