<?php

$db_host = 'localhost';
$db_name = 'devsnotes';
$db_user = 'postgres';
$db_pass = 'postgres';
$db_port = '5432';

$array = [
    'error' => '',
    'result' => []
];

$pdo = new PDO("pgsql:host=$db_host;port=$db_port;dbname=$db_name;user=$db_user;password=$db_pass ");