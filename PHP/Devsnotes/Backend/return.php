<?php
header('Access-Control-Allow-Origin: *'); // se quiser um dominio específico, colocar no lugar do *
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
header('Content-Type: application/json');


echo json_encode($array);
exit;