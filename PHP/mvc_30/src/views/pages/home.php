<?php $render('header'); ?>

<a href="<?=$base; ?>/novo"> Novo Usuário</a>
<br>

<table border="1" width="100%">
    <tr>
        <th>#</th>
        <th>Nome</th>
        <th>e-Mail</th>
        <th>Ações</th>
    </tr>
    <?php foreach($usuarios as $usuario): ?>
        <tr>
            <td><?=$usuario['id']?></td>
            <td><?=$usuario['nome']?></td>
            <td><?=$usuario['email']?></td>
            <td>
                <a href="<?=$base?>/usuario/<?=$usuario['id']?>/editar">
                    <img width="20" src="<?=$base;?>/assets/images/document.png" alt="Editar">
                </a>
                <a href="<?=$base?>/usuario/<?=$usuario['id']?>/excluir" onClick="return confirm('Deseja excluir este registro?')">
                    <img width="20" src="<?=$base;?>/assets/images/trash.png" alt="Excluir">
                </a>
            </td>
        </tr>
    <?php endforeach ?>
</table>

<?php $render('footer'); ?>