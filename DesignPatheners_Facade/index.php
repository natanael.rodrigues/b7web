<?php

require 'classes.php';

class Purchase {

    private $order;
    private $billing;
    private $shipping;

    public function __construct(OrderInterface $order, BillingInterface $billing, ShippingInterface $shipping){
        $this->order = $order;
        $this->billing = $billing;
        $this->shipping = $shipping;
    }

    public function finish(){
        $this->billing->chargeCreditCar();
        $this->order->setStatus($this->billing->getStatus());
        
        if ($this->order->isOK(){
            $this->shipping->addToPipeline($this->order);    
        }
    }
}

$purchase = new ($oder, $billing, $shipping);

$purchase->finish();