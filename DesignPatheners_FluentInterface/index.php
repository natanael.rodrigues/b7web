<?php

require 'classes.php';

class Person{
    private $name;
    private $lastName;

    public function setName($n){
        $this->name = $n;
        return $this;
    }

    public function setLastName($ln){
        $this->lastName = $ln;
        return $this;
    }

    public function getFullName(){
        return $this->Name . ' ' . $this->lastName;
    }
}

$person = new Person;

$person->setName('Natanael')->setLastName('Rodrigues');

echo $person->getFullName();