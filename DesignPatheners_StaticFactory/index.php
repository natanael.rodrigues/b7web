<?php
    final class StaticFactory{
        public static function make($type){
            if($type == 'number'){
                return new FormatNumber();    
            }

            if($type == 'string'){
                return new FormatString();    
            }
        }
    }

    interface FormatterInteface{
        public function format($n);
    }

    class FormatNumber implements FormatterInterface{
        public function format($n){
            echo 'Formatando numero ' . $n;
        }
    }

    class FormatString implements FormatterInterface{
        public function format($n){
            echo 'Formatando String ' . $n;
        }
    }

    // uso

    $formatter = StaticFactory::make("string");
    $formatter->format("Testndo 1,2,3...");
    