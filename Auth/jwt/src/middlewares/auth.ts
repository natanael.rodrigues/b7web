import { Request, Response, NextFunction } from 'express';
import JWT from 'jsonwebtoken';
import dotenv from 'dotenv';

import { User } from '../models/User'

dotenv.config();

export const Auth = {
    private: async ( req:Request, res:Response, next:NextFunction ) => {
        let success = false;

        // TODO: Fazer verificação de auth
        let hash = req.headers.authorization;

        if(hash) {
            
            const [authType, token] = hash.split(' ');

            if( authType === 'Bearer' ) {
                try {
                    const decoded = JWT.verify(
                        token, 
                        process.env.JWT_SECRET_KEY as string
                    )    

                    if(decoded) {
                        console.log(decoded);
                        success = true;
                    }
                } catch (error) {
                    console.log("Erro:", 'Deu erro no JWT / acess')
                }

            }
            
        }


        if(success){
            next();
        } else {
            res.status(403).json({error: 'Não autorizado'}); // not Autorized
        }
    }
}