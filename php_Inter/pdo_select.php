<?php

    // define local e o banco de acesso
    // MYSQL
    $dsn="mysql:dbname=blog_b7;host=127.0.0.1";
    // define usuario
    $dbuser="root";
    // define senha do usuario
    $dbpass="";

    try{
       
        $pdo = new PDO($dsn, $dbuser, $dbpass);

        $sql = "SELECT * FROM USUARIOS";

        $sql = $pdo->query($sql);

        if($sql->rowCount() > 0){
            
            foreach($sql->fetchAll() as $usuario){
                echo "Nome: " . $usuario["nome"] . " | " . $usuario["email"]. "<BR>";
            }

        }else{
            echo "Não há usuários cadastrados!";
        };

    }catch(PDOException $e){
        echo "Falhou: " . $e->getMessage();
    }

?>