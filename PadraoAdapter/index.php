<?php   
    class Pessoa {

        private $nome;
        private $idade;
        
        public function __construct($nome, $idade){
            $this->nome = $nome;
            $this->idade = $idade;
        }

        public function getNome(){
            return $this->nome;
        }

        public function setNome($nome){
            $this->nome = $nome;
        }
    }


    // Adapter da classe pessoa;

    class PessoaAdapter{
        private $sexo;
        private $pessoa;

        public function __construct (Pessoa $pessoa){
            $this->pessoa = $pessoa;    
        }


        public function setSexo($sexo){
            $this->sexo = $sexo;
        }
    }



    // instanciar uma classe Adapter
    $pessoa = new Pessoa();

    $pa = new PessoaAdapter($pessoa);


    