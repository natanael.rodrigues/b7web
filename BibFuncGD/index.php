<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Portal Modelo</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <div class="container">
        <?php

            $filename = 'assets\images\bg-face.png';

            $width = 200; // Largura
            $heigth = 200; // Altura

            list($largura_original, $altura_original ) = getimagesize($filename);

            $ratio =  $largura_original / $altura_original;

            if (($width / $heigth) > $ratio) {
                $width = $heigth * $ratio;
            } else {
                $heigth = $width / $ratio;
            }

            // cria uma imagem zerada.
            $image = imagecreatetruecolor($width, $heigth);
            // abre a imagem orignal
            $image_origin = imagecreatefrompng($filename);
            // copia a imagem
            imagecopyresampled($image, $image_origin, 0, 0, 0, 0, $width, $heigth, $largura_original, $altura_original );

            //joga na tela
            //header("Content-Type: image/png");
            //imagepng($image, null);
            
            imagepng($image, "assets/images/bg-face-redimencionada.png");

            echo "Imagem Redimencionada";

        ?>
    </div>

    <script type="text/javascript" src="assets/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
</body>
</html>
