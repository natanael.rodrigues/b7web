<?php
            $filename = 'assets\images\bg-face.png';

$width = 200; // Largura
$heigth = 200; // Altura

list($largura_original, $altura_original ) = getimagesize($filename);

$ratio =  $largura_original / $altura_original;

if (($width / $heigth) > $ratio) {
    $width = $heigth * $ratio;
} else {
    $heigth = $width / $ratio;
}

// cria uma imagem zerada.
$image = imagecreatetruecolor($width, $heigth);
// abre a imagem orignal
$image_origin = imagecreatefrompng($filename);
// copia a imagem
imagecopyresampled($image, $image_origin,0,0,0,0, $width, $heigth, $largura_original, $altura_original );

//joga na tela
header("Content-Type: image/png");
imagepng($image, null);



?>