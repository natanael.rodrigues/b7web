<?php

    $imagem = 'assets\images\bg-face.png';
    $imagem_mini ='assets\images\bg-face-redimencionada.png';
    $imagem_marcadagua ='assets\images\img-marcadagua.png';

    list($largura_original, $altura_original) = getimagesize($imagem);
    list($largura_mini, $altura_mini) = getimagesize($imagem_mini );

    $imagem_final = imagecreatetruecolor($largura_original, $altura_original);

    $imagem_original  = imagecreatefrompng($imagem);
    $imagem_mini = imagecreatefrompng($imagem_mini);

    imagecopy($imagem_final,$imagem_original, 0,0,0,0, $largura_original,$altura_original);

    imagecopy($imagem_final, $imagem_mini, 100,200,0,0,$largura_mini, $altura_mini);

   // header("Content-Type: image/png");

    imagepng($imagem_final, $imagem_marcadagua);

    echo "Gerado com sucesso!"

?>