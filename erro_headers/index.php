<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Portal Modelo</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <div class="container">
        <h1>Pagina 1</h1>

        <form method="POST">
            Digite "1234" para passar:<br/><br/>
            <input type="text" name="codigo" ><br/><Br/>

            <input type="submit" value="Enviar">

        </form>
    </div>

    <?php
        if (!empty($_POST['codigo'])){
            $codigo = $_POST['codigo'];

            if ($codigo == '1234') {
                header("location: pagina2.php");
            } else {
                echo "Acesso negado.";
            }

        }
    ?>

    <script type="text/javascript" src="assets/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
</body>
</html>
