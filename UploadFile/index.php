<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Portal Modelo</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <div class="container">
        <h1> Upload simples de arquivo</h1>
        <form action="recebedor.php" method="post" enctype="multipart/form-data">
            <input type="file" name="arquivo">

            <input type="submit" class='btn' value="Enviar">
        </form>
        <hr>

        <h1> Upload de vários arquivo</h1>

        <form action="recebedorVarios.php" method="post" enctype="multipart/form-data">
            <input type="file" name="arquivo[]" multiple>

            <input type="submit" class='btn' value="Enviar vários arquivos">
        </form>
    </div>

    <script type="text/javascript" src="assets/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
</body>
</html>
