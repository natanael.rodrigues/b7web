<?php

$book1 = new Book('Livro teste', 'Fulano');
$book2 = new Book('Livro O homem que calculava', 'cicrano');
$book3 = new Book('cronicas de gelo e fogo', 'beltrano');

$bookList = new BookList();

$bookList->addBook($book1);
$bookList->addBook($book2);
$bookList->addBook($book3);

echo "Total: ". $bookList->count();

$bookList->removeBook($book2);

echo "Total: ". $bookList->count();