<?php
    global $routes;
    $routes = array();

    $routes['/galeria/{id}'] = '/galeria/abrir/:id';
    $routes['/news/{id}'] = '/noticia/abrir/:id';
    $routes['/{titulo}'] =  '/noticia/abrir/:titulo';