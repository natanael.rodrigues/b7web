<?php

class anuncios extends model{
    public function getQuantidade(){
        $sql = "SELECT COUNT(*) as total FROM ANUNCIOS";

        $sql = $this->db->query($sql);

        if ($sql->rowCount() > 0) {
            $sql = $sql->fetch();

            return $sql['total'];
        } else {
            return 0;
        }
    }
}