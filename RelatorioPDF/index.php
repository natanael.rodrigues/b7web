<?php
    ob_start();
?>
<h1>Teste para relatorio</h1>
<h4> SubTitle </h4>

<?php
    $html = ob_get_contents();
    ob_end_clean();

    // Gera no pdf
    // composer require mpdf/mpdf
    require 'vendor/autoload.php';

    $mpdf = new mPDF();
    $mpdf->WriteHTML($html);
    //I = Abra no browser
    //D = Faz o Download
    //F = Salva no servidor
    $mpdf->Output('arquivo.pdf','F');

    //documentação mPDF
    // https://mpdf.github.io/

?>